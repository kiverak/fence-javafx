module com.fencejavafx {
    requires javafx.controls;
    requires javafx.fxml;
            
                            
    opens com.fencejavafx to javafx.fxml;
    exports com.fencejavafx;
}