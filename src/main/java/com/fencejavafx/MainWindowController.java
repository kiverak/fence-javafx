package com.fencejavafx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.math.BigDecimal;

public class MainWindowController {
    @FXML
    private Pane titlePane;
    @FXML
    private ImageView btnMinimize, btnClose;
    @FXML
    private TextField fenceQuantity;
    @FXML
    private TextField soundQuantity;
    @FXML
    private TextField plankQuantity;
    @FXML
    private TextField backfillQuantity;
    @FXML
    private TextField dismantlingQuantity;
    @FXML
    private CheckBox delivery;
    @FXML
    private CheckBox discount;
    @FXML
    private TextField fencePrice;
    @FXML
    private TextField soundPrice;
    @FXML
    private TextField plankPrice;
    @FXML
    private TextField backfillPrice;
    @FXML
    private TextField dismantlingPrice;
    @FXML
    private TextField deliveryPrice;
    @FXML
    private TextField discountPrice;
    @FXML
    private Label fenceResult;
    @FXML
    private Label soundResult;
    @FXML
    private Label plankResult;
    @FXML
    private Label backfillResult;
    @FXML
    private Label dismantlingResult;
    @FXML
    private Label deliveryResult;
    @FXML
    private Label discountResult;
    @FXML
    private Label sumResult;
    @FXML
    private Label totalResult;

    private double x, y;

    public void init(Stage stage) {
        titlePane.setOnMousePressed(mouseEvent -> {
            x = mouseEvent.getSceneX();
            y = mouseEvent.getSceneY();
        });
        titlePane.setOnMouseDragged(mouseEvent -> {
            stage.setX(mouseEvent.getScreenX() - x);
            stage.setY(mouseEvent.getScreenY() - y);
        });

        btnClose.setOnMouseClicked(mouseEvent -> stage.close());
        btnMinimize.setOnMouseClicked(mouseEvent -> stage.setIconified(true));

        addIntListenerToTextField(fenceQuantity);
        addIntListenerToTextField(soundQuantity);
        addIntListenerToTextField(plankQuantity);
        addIntListenerToTextField(backfillQuantity);
        addIntListenerToTextField(dismantlingQuantity);
        addDoubleListenerToTextField(fencePrice);
        addDoubleListenerToTextField(soundPrice);
        addDoubleListenerToTextField(plankPrice);
        addDoubleListenerToTextField(backfillPrice);
        addDoubleListenerToTextField(dismantlingPrice);
        addDoubleListenerToTextField(deliveryPrice);
        addDoubleListenerToTextField(discountPrice);

        calculateResult(fenceQuantity, fencePrice, fenceResult);
        calculateResult(soundQuantity, soundPrice, soundResult);
        calculateResult(plankQuantity, plankPrice, plankResult);
        calculateResult(backfillQuantity, backfillPrice, backfillResult);
        calculateResult(dismantlingQuantity, dismantlingPrice, dismantlingResult);
        calculateResult(delivery, deliveryPrice, deliveryResult);
        calculateResult(discount, discountPrice, discountResult);

    }

    private void addIntListenerToTextField(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() == 0) {
                textField.setText("0");
                return;
            }
            if (newValue.matches("\\d*")) return;
            textField.setText(newValue.replaceAll("[^\\d]", ""));
        });
    }

    private void addDoubleListenerToTextField(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() == 0) {
                textField.setText("0");
                return;
            }
            if (newValue.matches("\\d*")) return;
            newValue = newValue.replace(",", ".");
            newValue = newValue.replaceAll("[^0-9.]", "");
            if (newValue.matches("\\d*.") || newValue.matches("^[0-9]+(\\.[0-9]{1,2})")) {
                if (Double.parseDouble(newValue) >= 1 && newValue.charAt(0) == 0) {
                    newValue = newValue.substring(1);
                }

                textField.setText(newValue);
            } else {
                newValue = newValue.substring(0, newValue.length() - 1);
                textField.setText(newValue);
            }
        });
    }

    private void calculateResult (TextField quantity, TextField price, Label result) {
        quantity.setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                result.setText(new BigDecimal(quantity.getText()).multiply(new BigDecimal(price.getText())).toString());
                calculateSumAndTotalResults();
            }
        });

        price.setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                result.setText(new BigDecimal(quantity.getText()).multiply(new BigDecimal(price.getText())).toString());
                calculateSumAndTotalResults();
            }
        });
    }

    private void calculateResult(CheckBox checkBox, TextField price, Label result) {
        checkBox.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if (checkBox.isSelected()) {
                    result.setText(price.getText());
                } else {
                    result.setText("0");
                }
                calculateSumAndTotalResults();
            }
        });

        price.setOnKeyTyped(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if (checkBox.isSelected()) {
                    result.setText(price.getText());
                } else {
                    result.setText("0");
                }
                calculateSumAndTotalResults();
            }
        });
    }

    private void calculateSumAndTotalResults() {
        BigDecimal sum = new BigDecimal(fenceResult.getText());
        sum = sum.add(new BigDecimal(soundResult.getText()));
        sum = sum.add(new BigDecimal(plankResult.getText()));
        sum = sum.add(new BigDecimal(backfillResult.getText()));
        sum = sum.add(new BigDecimal(dismantlingResult.getText()));
        sum = sum.add(new BigDecimal(deliveryResult.getText()));
        sumResult.setText(sum.toString());
        totalResult.setText(sum.subtract(new BigDecimal(discountResult.getText())).toString());
    }
}